module.exports = {
  purge: ["./index.html", "./src/**/*.{vue,js,ts,jsx,tsx}"],
  darkMode: false,
  theme: {
    extend: {
      colors: {
        main: {
          DEFAULT: "#C90E11",
          dark: "#A90407",
        },
        brightgreen: {
          DEFAULT: '#44fb03',
        }
      },
      borderWidth: {
        6: "6px",
        10: "10px",
        12: "12px",
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
};
