import { createRouter, createWebHashHistory, createWebHistory, RouteRecordRaw } from 'vue-router'
import store from '@/store'

const routes: Array<RouteRecordRaw> = [
  {
    path: '/login',
    name: 'Login',
    component: () => import('../views/Login.vue')
  },
  {
    path: '/',
    name: 'Home',
    // component: Home
    component: () => import('../views/Home.vue')
  },
  {
    path: '/product',
    name: 'Product',
    component: () => import('../views/Product.vue'),
    beforeEnter: (to, from, next) => {
      if (!store.state.product) {
        next('/')
      } else {
        next()
      }
    }
  }
]

const router = createRouter({
  history: process.env.IS_ELECTRON ? createWebHashHistory() : createWebHistory(process.env.BASE_URL),
  routes
})

router.beforeEach((to, from, next) => {
  const isAuthenticated = store.state.isAuthenticated;
  if (to.name !== "Login" && !isAuthenticated) next({ name: "Login" });
  else next();
});

export default router
