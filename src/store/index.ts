import { createStore } from "vuex";

export default createStore({
  state: {
    isLoading: false,
    product: null,
    isAuthenticated: false,
  },
  mutations: {
    setProduct(state, product: any) {
      state.product = product;
    },
    setIsLoading(state, bool: boolean) {
      state.isLoading = bool;
    },
    setAuthenticated(state, bool: boolean) {
      state.isAuthenticated = bool;
    },
  },
  actions: {
    setProduct(context, payload) {
      context.commit("setProduct", payload.product);
    },
    setIsLoading(context, payload) {
      context.commit("setIsLoading", payload);
    },
    setAuthenticated(context, payload) {
      context.commit("setProduct", payload);
    },
  },
  modules: {},
});
