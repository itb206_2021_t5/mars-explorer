import { createApp } from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";
import './utilities/fontawesome'

import "./styles/index.css";

createApp(App)
  .component("Fa", FontAwesomeIcon)
  .use(store)
  .use(router)
  .mount("#app");
