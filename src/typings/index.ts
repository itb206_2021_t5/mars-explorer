export interface SizeOption {
  pid: string;
  qty: number;
  size_name: string;
}

export interface Product {
  barcode: string;
  category_name: string;
  options: SizeOption[];
  pcid: string;
  pid: string;
  price: number;
  product_desc: string;
  product_images: string;
  product_name: string;
  brand_name: string;
  qty: number;
  retail_price: number;
  size_name: string;
  subcategory_name: string;
}

export interface Store {
  district: string
  fax: string
  qty: string
  shop_address: string
  shop_address_chi: string
  shop_code: string
  shop_id: string
  shop_name: string
  shop_name_chi: string
  tel: string
}