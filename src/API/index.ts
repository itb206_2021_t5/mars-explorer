import { Methods } from "@/typings/api";
import request from "./request";

const API = {
  fetchProductDetail(barcode: string) {
    return request(Methods.Get, `/product/detail/${barcode}`);
  },

  fetchStock(productId: string) {
    return request(Methods.Get, `/stock/${productId}`)
  }
};

export default API;
