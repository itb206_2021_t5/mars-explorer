import { library } from "@fortawesome/fontawesome-svg-core";
import { faTimes, faArrowLeft } from "@fortawesome/free-solid-svg-icons";

library.add(faTimes);
library.add(faArrowLeft);